import { NextApiRequest, NextApiResponse } from 'next'
import { ICartProduct } from '../../interfaces'
import Stripe from 'stripe'

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY || '', {
  apiVersion: '2020-08-27',
})

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === 'POST') {
    try {
      const params: Stripe.Checkout.SessionCreateParams = {
        submit_type: 'pay',
        payment_method_types: ['card'],
        billing_address_collection: 'auto',
        shipping_options: [
          { shipping_rate: 'shr_1Krtr8IzkRFEJ0MzjTXp61am' },
          { shipping_rate: 'shr_1KrtrnIzkRFEJ0MzHRJebnkv' },
        ],
        line_items: req.body.map(
          (
            product: ICartProduct
          ): Stripe.Checkout.SessionCreateParams.LineItem => {
            const img = product.image[0].asset._ref
            const newImage = img
              .replace(
                'image-',
                `https://cdn.sanity.io/images/${process.env.SANITY_PROJECT_ID}/${process.env.NEXT_PUBLIC_SANITY_DATASET}`
              )
              .replace('-webp', '.webp')
            return {
              price_data: {
                currency: 'usd',
                product_data: {
                  name: product.name,
                  images: [newImage],
                },
                unit_amount: product.price * 100,
              },
              adjustable_quantity: {
                enabled: true,
                minimum: 1,
              },
              quantity: product.quantity,
            }
          }
        ),
        mode: 'payment',
        success_url: `${req.headers.origin}/success`,
        cancel_url: `${req.headers.origin}/?canceled=true`,
      }
      const session = await stripe.checkout.sessions.create(params)
      res.status(200).json(session)
    } catch (err) {
      res
        .status((err as Stripe.StripeRawError).statusCode || 500)
        .json((err as Stripe.StripeRawError).message)
    }
  } else {
    res.setHeader('Allow', 'POST')
    res.status(405).end('Method Not Allowed')
  }
}
