import Link from 'next/link'
import { IBanner } from '../interfaces'
import { urlFor } from '../lib/client'

interface Props {
  banner: IBanner
}

const HeroBanner = ({ banner }: Props) => {
  return (
    <div className="hero-banner-container">
      <div>
        <p className="beats-solo">{banner.smallText}</p>
        <h3>{banner.midText}</h3>
        <h1>{banner.largeText1}</h1>
        <img
          src={urlFor(banner.image).url()}
          alt={banner.product}
          className="hero-banner-image"
        />
        <div>
          <Link href={`/product/${banner.product}`}>
            <button type="button">{banner.buttonText}</button>
          </Link>
          <div className="desc">
            <h5>Description</h5>
            <p>{banner.description}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default HeroBanner
